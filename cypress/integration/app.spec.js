describe('App test',()=>{
    it('Visit my local host', ()=> {
        cy.visit('/');
        cy.get('input[type="number"]')
            .type('20')
            .should('have.value','20');
        cy.get('div.toggle-switch > input[id="lowercase"]')
                .focus().check({force: true})
         
        cy.get('div.toggle-switch > input[id="uppercase"]')
            .focus().check({force: true})
        cy.get('div.toggle-switch > input[id="number"]')
            .check({force: true})
        cy.get('div.toggle-switch > input[id="symbol"]')
            .check({force: true})
        cy.get('button[name="btn btn-large"]')
            .click()
        cy.get('span#result').should(($span) => {
            const text = $span.text()
            expect(text).not.to.include('ÅÄÖ')
        })
    })
    it('Activate the alert box', ()=> {
        cy.visit('/');
        cy.get('input[type="number"]')
            .type('20')
            .should('have.value','20');
        cy.get('div.toggle-switch > input[id="lowercase"]').focus().check({force: true})

        cy.get('button[name="btn btn-large"]')
            .click()
        cy.get('span#result').should(($span) => {
            const text = $span.text()
            expect(text).not.to.include('ÅÄÖ')
        })
        const stub = cy.stub()  
        cy.on ('window:alert', stub)
        cy.get('button').contains('📋').click()
    .then(() => {   expect(stub.getCall(0)).to.be.calledWith('Password copied to clipboard') })  
    })
})