import React from 'react';


const Button = (props) => (
  <button 
    className={props.name} 
    id={props.id}
    name={props.name}
  >
    {props.desc}
  </button>
)


export default Button;
