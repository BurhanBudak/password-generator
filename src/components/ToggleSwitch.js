import React,{Component} from 'react';
import PropTypes from "prop-types";
import "./ToggleSwitch.css";

  class ToggleSwitch extends Component {
    state = {
      checked: this.props.defaultChecked
    };

    onChange = e => {
      this.setState({
        checked: e.target.checked
      });
      if (typeof this.props.onChange === "function"){ 
        this.props.onChange(this.props.name, e.target.checked)
      };
    };

    render() {
      return (
        <div className="setting">
          <p>{this.props.desc}</p>
         
        <div className={"toggle-switch"} >
          <input
            type="checkbox"
            name={this.props.Name}
            className="toggle-switch-checkbox"
            id={this.props.id}
            // checked={this.props.currentValue}
            defaultChecked={this.props.defaultChecked}
            onChange={this.onChange}
            // disabled={this.props.disabled}
          />
            <label className="toggle-switch-label" htmlFor={this.props.id}>
              <span
                className={"toggle-switch-inner"}
                data-yes={this.props.Text[0]}
                data-no={this.props.Text[1]}
              />
              <span className={"toggle-switch-switch"}/>
            </label>
        </div>
        </div>
      );
    }
    // Set text for rendering.
    static defaultProps = {
      Text: ["Yes", "No"]
    };
  }
  
  ToggleSwitch.propTypes = {
    id: PropTypes.string.isRequired,
    Name: PropTypes.string,
    onChange: PropTypes.func,
    defaultChecked: PropTypes.bool,
    Small: PropTypes.bool,
    currentValue: PropTypes.bool,
    disabled: PropTypes.bool
  };
  
  export default ToggleSwitch;