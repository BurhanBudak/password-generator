import React from "react";
import InputNumber from "./InputNumber";
import ToggleSwitch  from "./ToggleSwitch";
import Button from "./Button";
import generatePassword from "../utils/helpers";

class FormSubmit extends React.Component {
    constructor(){
        super();
        this.state = {
            lowercase: false,
            uppercase: false,
            number: false,
            symbol: false,
            // emoji: false,
            amount: "",
        }
    }
    handleInputChange = (value) =>{
        this.setState({
            amount:Number(value)
        })
    }
    submitForm = (event) => {
        event.preventDefault()
        let a = generatePassword(this.state)
        this.props.onPasswordGenerate(a)
        
    }
    onToggleSwitchChanged = (name, value) => {
        this.setState({
            [name]: value
        })
    }
    render(){
        return(
        <form className="settings" onSubmit={this.submitForm}>
        <InputNumber name="amount"  onChange={this.handleInputChange}/>
        <ToggleSwitch name="lowercase" id="lowercase" desc="Inlcude lowercase letters" onChange={this.onToggleSwitchChanged} defaultChecked={false}/>
        <ToggleSwitch name="uppercase" id="uppercase" desc="Include uppercase letters" onChange={this.onToggleSwitchChanged} defaultChecked={false}/>
        <ToggleSwitch name="number" id="number"  desc="Include numbers" onChange={this.onToggleSwitchChanged} defaultChecked={false}/>
        <ToggleSwitch name="symbol" id="symbol"  desc="Include symbols" onChange={this.onToggleSwitchChanged} defaultChecked={false}/>
        {/* <ToggleSwitch name="emoji" id="emoji"  desc="Include emojis 😃" onChange={this.onToggleSwitchChanged}/> */}
        <Button type="submit" name="btn btn-large" id="generate" desc="Generate password"/>
        </form>
        )
    }  
}
export default FormSubmit;