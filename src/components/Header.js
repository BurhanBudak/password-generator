import React from 'react';


const Header = (props) => (
  <header className="top">
  <h2 className="title">
        {props.title}
  </h2>
</header>
)

export default Header;
