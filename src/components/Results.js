import React from 'react';
import clipboard from "../utils/clipboard";

class Results extends React.Component {
  
  clipBoard = () => {
    
    clipboard(this.props.finalPassword)
    
  };
  render(){
    return(
        <div className="result-container">
          <span id="result">{this.props.finalPassword}</span>
		      <button className="btn" onClick={this.clipBoard}>
                <span role="img" aria-label="clipboard">📋</span>
		      </button>
	      </div>
    )
  }
}


export default Results;