import React from 'react';
import './App.css';
import Header from "./Header";
import Results from "./Results";
import FormSubmit from "./FormSubmit";

class App extends React.Component {
  state = {
    finalPassword: ''
  }

  onPasswordGenerate = (password) => {
    this.setState({
      finalPassword: password
    })
  }
  render(){
    return(
      <div className="container">
        <Header title="Password generator"/>
        <Results finalPassword={this.state.finalPassword}/>
        <FormSubmit onPasswordGenerate={this.onPasswordGenerate}/>
      </div>
    )
  }
}


export default App;
