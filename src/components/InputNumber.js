import React from 'react';

class InputNumber extends React.Component {
    // constructor(props) {
    //   super(props);
    //   this.state = {
    //     amount: ''
    //   }
    // }
    handleChange= (event)=> {
      // const amount = (event.target.validity.valid) ? event.target.value : this.state.lenght;
      // this.setState({ amount: Number(event.target.value) });
      this.props.onChange(event.target.value)
    }
    
    
    render() {
      return (
          <div className="setting">
			<label>Password length</label>	
          <input 
          type="number" 
          id={this.props.name} 
          onChange={this.handleChange} 
          // value={this.state.amount} 
          pattern="[0-9]*"
          min='4'
           max='20'
           />
		</div>
      )
    }
  }

export default InputNumber;
