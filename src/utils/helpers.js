
const randomFunc = {
	lowercase: getRandomLower,
	uppercase: getRandomUpper,
	number: getRandomNumber,
	symbol: getRandomSymbol,
	// emoji: getRandomEmoji
}

function getRandomLower() {
	let l = String.fromCharCode(Math.floor(Math.random() * 26) + 97); 
	
	return l;
}

function getRandomUpper() {
	let u = String.fromCharCode(Math.floor(Math.random() * 26) + 65);
	
	return u
}

function getRandomNumber() {
	let n = (+String.fromCharCode(Math.floor(Math.random() * 10) + 48));
	
	return n
}

function getRandomSymbol() {
	const symbols = '!@#$%^&*(){}[]=<>/,.+'
	let s =  symbols[Math.floor(Math.random() * symbols.length)];
	return s
}

// function getRandomEmoji() {
// 	let emojiRange = [
// 		[128513, 128591], [9986, 10160], [128640, 128704]
// 	];
// 	let range = emojiRange[Math.floor(emojiRange.length * Math.random())]
// 	let unicode = Math.floor(Math.random()*range[0])+range[1]
// 	// return "\\u" + unicode + "";
// 	return '😀'
// }




export function generatePassword(params) {
	let password = '';
	
	const typesCount = params.lowercase + params.uppercase + params.number + params.symbol;
	let lowercase= params.lowercase;
	let uppercase = params.uppercase;
	let number = params.number;
	let symbol = params.symbol;
	// let emoji = params.emoji;
	const typesArr =  [{lowercase},{uppercase},{number},{symbol}].filter(item => Object.values(item)[0]);
	// Doesn't have a selected type
	if(typesCount === 0) {
		return '';
	}
	
	// create a loop
	for(let i=0; i<params.amount; i+=typesCount) {
		// eslint-disable-next-line
		typesArr.forEach(type => {
			const funcName = Object.keys(type)[0];
			password += randomFunc[funcName]();
		});
	}
	if(params.amount >= 4){
		const finalPassword = password.slice(0, params.amount);
		return finalPassword
	}
	else{
		return "hunter2"
	}
	
}
export default generatePassword



