export function clipboard(finalPassword){
	
	const textarea = document.createElement('textarea');
    const password = finalPassword;
    
    if(password === ''){ 
      return false
    }
    
      textarea.value = password;
      document.body.appendChild(textarea);
      textarea.select();
      document.execCommand('copy');
      textarea.remove();
      alert('Password copied to clipboard');
}
export default clipboard