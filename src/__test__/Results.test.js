import React from 'react';
import { mount } from 'enzyme';
import  Results  from "../components/Results";
import clipboard from "../utils/clipboard";

test('Render result span',()=>{
    const wrapper = mount(<Results/>)
    expect(wrapper.find('span')).toHaveLength(2)
})
test('Render result button',()=>{
    const wrapper = mount(<Results/>)
    expect(wrapper.find('button')).toHaveLength(1)
})
test('Activate alertbox if there is a password',()=>{
    const alertSpy = jest.fn();
    window.alert = alertSpy;
    window.document.execCommand = jest.fn();
    const wrapper = mount(<Results/>)
    wrapper.find('button').simulate('click');
    expect(alertSpy).toHaveBeenCalled();
})
test('value of span clipboard', () =>{
    const wrapper = mount(<Results/>)
    expect(wrapper.text()).toEqual('📋')
});
test('value of span results', () =>{
    const wrapper = mount(<Results/>)
    const span = wrapper.find('span#result')
    expect(span.text()).toEqual('');
});
test('value of span after click', () =>{
    const wrapper = mount(<Results/>)
    
    wrapper.setProps({finalPassword:'hunter2'})
    expect(wrapper.props().finalPassword).toEqual('hunter2');
});

// test('test that external func clipboard was called', () => {
//     const onClick = jest.fn();
//     const wrapper = mount(<Results onClick={onClick} />);
//     wrapper.find('button.btn').simulate('click');
//     expect(onClick).toHaveBeenCalled();
// })
