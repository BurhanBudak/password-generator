import React from 'react';
import { mount } from 'enzyme';
import  App  from "../components/App";

test('Render every child in App', () => {
    const wrapper = mount(<App/>)
    expect(wrapper.find('header')).toHaveLength(1);
    expect(wrapper.find('form')).toHaveLength(1);
    expect(wrapper.find('div.result-container')).toHaveLength(1);
    wrapper.find('form').simulate('submit','form');
    wrapper.setState({finalPassword:'hunter2'})
    expect(wrapper.state().finalPassword).toBe('hunter2')
});