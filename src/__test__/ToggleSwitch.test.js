import React from 'react';
import { mount } from 'enzyme';
import ToggleSwitch from '../components/ToggleSwitch';

test('test that toggleswitch renders', () => {
    const wrapper = mount(<ToggleSwitch id="lowercase" />);
    expect(wrapper.find('input')).toHaveLength(1);
});

test('test that state is set true and then changed to false with simulate', () => {
    const wrapper = mount(<ToggleSwitch id="lowercase" />);
    wrapper.setState({checked: true})
    wrapper.find('input#lowercase').simulate('change');
    expect(wrapper.state().checked).toBe(false);
});

test("should fail if id is not added", () => {
    const wrapper = mount(<ToggleSwitch id="lowercase"/>);
    expect(wrapper.find("label#lowercase")).toHaveLength(0);
  });

test("shouldrun if id is added", () => {
    const wrapper = mount(<ToggleSwitch id="lowercase"/>);
    expect(wrapper.find("label")).toHaveLength(1);
});

test('test that toggleswitch lowercase is true', () => {
    const wrapper = mount(<ToggleSwitch id="lowercase" />);
    // wrapper.find('input#lowercase').simulate('change');
    wrapper.setState({checked:true})
    expect(wrapper.state().checked).toBe(true);
});
test('test that toggleswitch uppercase is true', () => {
    const wrapper = mount(<ToggleSwitch id="uppercase" />);
    // wrapper.find('input#lowercase').simulate('change');
    wrapper.setState({checked:true})
    expect(wrapper.state().checked).toBe(true);
});
test('test that toggleswitch number is true', () => {
    const wrapper = mount(<ToggleSwitch id="number"/>);
    // wrapper.find('input#lowercase').simulate('change');
    wrapper.setState({checked:true})
    expect(wrapper.state().checked).toBe(true);
});
test('test that toggleswitch symbol is true', () => {
    const wrapper = mount(<ToggleSwitch id="symbol" />);
    // wrapper.find('input#lowercase').simulate('change');
    // wrapper.instance().checked = true;
    // wrapper.simulate('change');
    wrapper.setState({checked:true})
    expect(wrapper.state().checked).toEqual(true);
});

test('test that external func was called', () => {
    const onChange = jest.fn();
    const wrapper = mount(<ToggleSwitch id="lowercase" onChange={onChange} />);
    wrapper.find('input#lowercase').simulate('change');
    expect(wrapper.state().checked).toBe(false)
    expect(onChange).toHaveBeenCalled();
})

test('check if component has the class', () => {
    const id = "lowercase"
    const wrapper = mount(<ToggleSwitch id={id}/>);
    wrapper.find('input#lowercase').simulate('change');
    expect(wrapper.find('span.toggle-switch-inner').hasClass('toggle-switch-disabled')).toBe(false)
})
test('check if div is true', () => {
    const id = "lowercase"
    const wrapper = mount(<ToggleSwitch id={id}/>);
    const parent =  wrapper.find('div.toggle-switch');
    expect(parent.contains(
        <label className="toggle-switch-label" htmlFor={id}>
              <span
                className={"toggle-switch-inner toggle-switch-disabled"}
                data-yes={"YES"}
                data-no={"NO"}
              />
              <span className={"toggle-switch-switch toggle-switch-disabled"} />
            </label>
    )).toEqual(false);
})