import {generatePassword, clipboard} from "../utils/helpers";
import Results from "../components/Results";
test('Generate a random password but isnt hunter2', ()=>{
    let params = {
        lowercase: true,
        uppercase: true,
        number: true,
        symbol: true,
        amount: 20
    }
   
    const password =  generatePassword(params)
    expect(password).not.toBe('hunter2')
})
test('Generate a random password but its hunter2', ()=>{
    let params = {
        lowercase: true,
        uppercase: true,
        number: true,
        symbol: true,
        amount: 0
    }
   
    const password =  generatePassword(params)
    expect(password).toBe('hunter2')
})

