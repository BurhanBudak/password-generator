import React from 'react';
import { mount } from 'enzyme';
import InputNumber from '../components/InputNumber';

test('Render inputfield',()=>{
    const wrapper = mount(<InputNumber id='amount'/>)
    expect(wrapper.find('input')).toHaveLength(1);
})

// test('Inputfields value shuld be empty' , () => {
//     const wrapper = mount(<InputNumber id='amount'/>)
//     expect(wrapper.state().amount).toBe('');
// })
test('Inputfields value should be 20' , () => {
    const wrapper = mount(<InputNumber id='amount'/>)
    
    wrapper.find("input").instance().value = "10"
    
    expect(wrapper.find("input").instance().value).toBe('10')
})
test('Inputfields value should not be string' , () => {
    const wrapper = mount(<InputNumber id='amount'/>)
    
    wrapper.find("input").instance().value = "10"
    
    expect(wrapper.find("input").instance().value).not.toBe('abc')
})

// test('Inputfields value should be 20' , () => {
//     const wrapper = mount(<InputNumber id='lenght'/>)
//     wrapper.simulate('change').setState({amount:20})
//     expect(wrapper.state().amount).toBe(20);
// })
test('test that external func was called', () => {
    const onChange = jest.fn();
    const wrapper = mount(<InputNumber onChange={onChange}/>) 
    // const span = wrapper.find('span#result')
    wrapper.find('input').simulate('change');
    expect(onChange).toHaveBeenCalled();
})