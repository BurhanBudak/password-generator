import React from 'react';
import { mount } from 'enzyme';
import  FormSubmit  from "../components/FormSubmit";
;

test('render form', () => {
    const wrapper = mount(<FormSubmit/>)
    expect(wrapper.find('form')).toHaveLength(1);
})
test('find inputfield. submitbutton and all toggles in form', () => {
    const wrapper = mount(<FormSubmit/>)
    expect(wrapper.find('button')).toHaveLength(1);
    expect(wrapper.find('input#amount')).toHaveLength(1);
    expect(wrapper.find('input')).toHaveLength(5);
});
test('find inputfield lowercase and change', () => {
    const wrapper = mount(<FormSubmit/>)
    wrapper.setState({lowercase:true})
    expect(wrapper.state().lowercase).toEqual(true)
})
test('find submit button and submit state', () => {
    const wrapper = mount(<FormSubmit/>)
    wrapper.setState({lowercase:true, amount:12})
    wrapper.find('button').simulate('click');
    expect(wrapper.state().lowercase).toEqual(true)
})
test('find submit button and submit state of amount', () => {
   
    const wrapper = mount(<FormSubmit/>)
    wrapper.setState({amount:12})
    wrapper.find('button').simulate('click');
    expect(wrapper.state().amount).toBe(12)
})
test('test that the prop function in child InputNumber is called', () => {
    
    const wrapper = mount(<FormSubmit/>)
    const intance = wrapper.instance();
    expect(wrapper.state().amount).toBe('')
    intance.handleInputChange(10)
    expect(wrapper.state().amount).toBe(10)
});

test('test that prop function onToggleSwitchChanged is called', () => {
    let lowercase = "lowercase"
    const wrapper = mount(<FormSubmit/>);
    const intance = wrapper.instance();
    expect(wrapper.state().lowercase).toBe(false)
    intance.onToggleSwitchChanged(lowercase,true)
    expect(wrapper.state().lowercase).toBe(true)
})